import React from 'react';
import './App.css';
import { useForm } from 'react-hook-form';

class Data {
    geoX: number = 0;
}

function App() {
    var form = useForm<Data>({ mode: 'onChange', defaultValues: new Data() });
    const {
        register,
        handleSubmit,
        setValue,
        reset,
        formState: { isDirty },
    } = form;
    const onSubmit = (data: any) => {
        console.log(data);
        reset(data);
    };

    return (
        <>
            <link
                href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
                rel="stylesheet"
                integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
                crossOrigin="anonymous"
            ></link>
            <div className="container">
                <form onSubmit={handleSubmit(onSubmit)}>
                    <Component form={form} />
                    <input className="form-group" type="submit" disabled={!isDirty} />
                </form>
            </div>
        </>
    );
}

interface ComponentProps {
    form: any;
}

const Component = (props: ComponentProps) => {
    console.log(props.form);
    const { register, setValue } = props.form;

    const update = async () => {
        setValue('geoX', 52.12344, { shouldDirty: true });
    };

    return (
        <>
            <div className="form-group">
                <label className="control-label">GeoX:</label>
                <div className="input-group">
                    <input type="number" className="form-control" {...register('geoX')} />
                </div>
            </div>
            <button type="button" onClick={() => update()}>
                Click Me
            </button>
        </>
    );
};

export default App;
